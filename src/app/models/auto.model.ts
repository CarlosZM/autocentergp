

export class AutoModel {

    caracteristicas:String;
    capacidad: number;
    color: string;
    dias: number;
    estado: boolean = true;
    fecha: Date;
    id: string;
    imagen: string;
    marca: string;
    modelo: string;
    monto: number;
    placas: string;

}