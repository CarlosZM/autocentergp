

export class UsuarioModel {
    apellido1: string;
    apellido2: string;
    direccion: string;
    email: string;
    id: string;
    licencia: string;
    nombre: string;
    password: string;
    telefono: number;
    tipo: string = "user";
}
