export class RentaModel {

    id: string;
    email: string; //usuario.email
    nombre: string; //usuario.nombre
    apellido1: string; //usuario.apellido1
    apellido2: string; //usuario.apellido1
    telefono: number; //usuario.telefono
    direccion: string; //usuario.direccion
    licencia: string; //usuario.licencia
    marca: string; //auto.marca + auto.modelo
    modelo: string; //auto.marca + auto.modelo
    capacidad: number; //auto.capacidad
    placas: string; //auto.placas
    color: string; //auto.color
    caracteristicas: string; //auto.caracteristicas
    imagen: string; //auto.imagen
    monto: number; //auto.monto
    dias: number; //Cantidad de dias
    total: number; //dias * monto
    fecha: Date; //Fecha de incio
    
}