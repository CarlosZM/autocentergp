import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TicketService } from 'src/app/ticket-service.service';

//Fecha
import { registerLocaleData } from '@angular/common';
import localEs from '@angular/common/locales/es';
registerLocaleData(localEs);

// npm install angularfire2 firebase --save
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RegistroComponent } from './pages/user/registro/registro.component';
import { HomeComponent } from './pages/public/home/home.component';
import { LoginComponent } from './pages/public/login/login.component';
import { AutoComponent } from './pages/admin/auto/auto.component';
import { AutosComponent } from './pages/admin/autos/autos.component';
import { UsuariosComponent } from './pages/admin/usuarios/usuarios.component';
import { CatalogoComponent } from './pages/user/catalogo/catalogo.component';
import { RentasComponent } from './pages/admin/rentas/rentas.component';
import { RentaComponent } from './pages/admin/renta/renta.component';
import { NavbarComponent } from './pages/public/navbar/navbar.component';
import { TicketComponent } from './pages/user/ticket/ticket.component';
import { CatalogComponent } from './pages/public/catalog/catalog.component';

const firebaseConfig = {
  apiKey: "AIzaSyB0bNoUfWWSIhGs4tjbtG41cQedztCRDIw",
  authDomain: "autocenter-35144.firebaseapp.com",
  databaseURL: "https://autocenter-35144.firebaseio.com",
  projectId: "autocenter-35144",
  storageBucket: "autocenter-35144.appspot.com",
  messagingSenderId: "178157940103",
  appId: "1:178157940103:web:fc494e8a3cb3f869b3f722",
  measurementId: "G-WM0PRR8KG1"
};

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    HomeComponent,
    LoginComponent,
    AutoComponent,
    AutosComponent,
    UsuariosComponent,
    CatalogoComponent,
    RentasComponent,
    RentaComponent,
    NavbarComponent,
    TicketComponent,
    CatalogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es',
    },
    TicketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
