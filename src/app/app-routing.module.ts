import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/public/home/home.component';
import { RegistroComponent } from './pages/user/registro/registro.component';
import { LoginComponent } from './pages/public/login/login.component';
import { CatalogoComponent } from './pages/user/catalogo/catalogo.component';
import { CatalogComponent } from './pages/public/catalog/catalog.component';
import { AutosComponent } from './pages/admin/autos/autos.component';
import { AutoComponent } from './pages/admin/auto/auto.component';
import { UsuariosComponent } from './pages/admin/usuarios/usuarios.component';
import { RentasComponent } from './pages/admin/rentas/rentas.component';
import { RentaComponent } from './pages/admin/renta/renta.component';
import { AuthGuard } from './guards/auth.guard';
import { TicketComponent } from './pages/user/ticket/ticket.component';


const routes: Routes = [
  { path: 'home'    , component: HomeComponent, canActivate: [ AuthGuard ] },
  { path: 'registro', component: RegistroComponent },
  { path: 'login'   , component: LoginComponent },
  { path: 'catalogo'   , component: CatalogoComponent, canActivate: [ AuthGuard ] },
  { path: 'catalog'   , component: CatalogComponent },
  { path: 'auto/nuevo'   , component: AutoComponent, canActivate: [ AuthGuard ] },
  { path: 'autos'   , component: AutosComponent, canActivate: [ AuthGuard ] },
  { path: 'usuarios'   , component: UsuariosComponent, canActivate: [ AuthGuard ] },
  { path: 'rentas'   , component: RentasComponent, canActivate: [ AuthGuard ] },
  { path: 'ticket'   , component: TicketComponent, canActivate: [ AuthGuard ] },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
