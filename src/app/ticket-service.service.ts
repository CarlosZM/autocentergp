import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class TicketService{
    private parametro = new BehaviorSubject(null);
    constructor(){}

    setParametro(parametro: any){
        this.parametro.next(parametro); 
    }
    getParametro(){
        return this.parametro.asObservable()
    }

    
}
