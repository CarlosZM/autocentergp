import { Component, OnInit } from '@angular/core';
import { AutosService } from 'src/app/services/autos.service';
import { AutoModel } from 'src/app/models/auto.model';
import { Router } from '@angular/router';
import { RentasService } from 'src/app/services/rentas.service';
import { RentaModel } from 'src/app/models/renta.model';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { TicketService } from 'src/app/ticket-service.service';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  autos: AutoModel[] = [];
  autoid = new AutoModel;
  usuario = new UsuarioModel;
  rentasReg: RentaModel;
  cargando: boolean;
  displayDialog: boolean;

  esDecimal: boolean;
  esMayor: boolean;
  totalValido: boolean;
  Deshabilitado: boolean = true;
  fecha: Date = new Date();

  modeloF: String;
  marcaF: String;
  minF: number;
  maxF: number;
  capF: number;
  renta: RentaModel;
  ticket: TicketService;

  constructor( private autosService: AutosService,
    private router: Router,
    private crearRentas:RentasService,
    private rentasService: RentasService,
    private ticketService: TicketService) { }

    ngOnInit(): void {

      this.cargando = true;
  
      this.autosService.col$('autos')
        .subscribe( resp => {
          this.autos = resp;
          this.cargando = false;
        });
    }
  
    rentarAuto(auto){
  
      this.rentasService.crearRenta("rentas",auto)    
      .then(response => {
        console.log(response)
        window.sessionStorage.setItem("rentaObtenida", JSON.stringify(auto))
    })
      .catch(err => console.log(err))
      this.autosService.updateEstado(`autos/${auto.id}`,{
        estado: false
      })
  
        Swal.fire({
          title: `Reservacion Completada`,
          icon: 'success'
        });
  
        setTimeout(() => {
          this.router.navigateByUrl('/ticket');
        }, 1000);
  
    }
  
    getAutos() {
      this.autosService.col$('autos')
      .subscribe( resp => {
        this.autos = resp;
        this.cargando = false;
      });
    }
  
    filtrarModelo(modeloF){
      this.modeloF=modeloF
      this.autosService.col$('autos', ref => ref.where('modelo','==',this.modeloF))
      .subscribe( resp => {
        console.log(resp);
        this.autos = resp;
        this.cargando = false;
      });
    }
  
    filtrarMarca(marcaF){
      this.marcaF=marcaF
      this.autosService.col$('autos', ref => ref.where('marca','==',this.marcaF.toUpperCase()))
      .subscribe( resp => {
        console.log(resp);
        this.autos = resp;
        this.cargando = false;
      });
    }
  
    filtrarMonto(minF,maxF){
      this.minF = minF
      this.maxF = maxF
      this.autosService.col$('autos', ref => ref.where('monto','>', this.minF ).where('monto','<', this.maxF))
      .subscribe( resp => {
        console.log(resp);
        this.autos = resp;
        this.cargando = false;
      });
    }
    
    filtrarCapacidad(capF){
      this.capF = parseInt(capF);
        this.autosService.col$('autos', ref => ref.where('capacidad','==', this.capF))
        .subscribe( resp => {
          console.log(resp);
          this.autos = resp;
          this.cargando = false;
        });
    }
  
    filtrarDisponibles(){
      this.autosService.col$('autos', ref => ref.where('estado','==', true))
      .subscribe( resp => {
        console.log(resp);
        this.autos = resp;
        this.cargando = false;
      });
    }
  
    // filtrar( modeloF, marcaF, minF, maxF, capF) {
  
    //   this.modeloF = modeloF;
    //   this.marcaF = marcaF;
    //   this.minF = minF;
    //   this.maxF = maxF;
    //   this.capF = capF;
  
    //     this.autosService.col$('autos', ref => ref.where('modelo','<=', this.modeloF)
    //     .where('marca','==', this.marcaF)
    //     .where('monto','>', this.minF )
    //     .where('monto','<', this.maxF)
    //     .where('capacidad','==', this.capF))
  
    //   .subscribe( resp => {
    //     console.log(resp);
    //     this.autos = resp;
    //     this.cargando = false;
    //   });
    // }
  
    valida(auto){
      if (auto.dias === auto.dias && auto.dias % 1 !== 0) {
        this.esDecimal = true;
        this.Deshabilitado = true;
      } else {
        this.esDecimal = false;
        this.Deshabilitado = false;
      }
    }
  
    mayorSiete(auto){
      if(auto.dias>7){
        this.esMayor = true;
        this.Deshabilitado = true;
      } else {
        this.esMayor = false
        this.Deshabilitado = false;
      }
    }
  
    impTotal(auto){
      if(auto.dias<1) {
        this.totalValido = false;
        this.Deshabilitado = true;
      } else if(auto.dias>7) {
        this.totalValido = false;
        this.Deshabilitado = true;
      } else if (auto.dias === auto.dias && auto.dias % 1 !== 0) {
        this.totalValido = false;
        this.Deshabilitado = true;
      } else {
        this.totalValido = true;
        this.Deshabilitado = false;
      }
    }
  
    selectP(event: Event) {
        this.displayDialog = true;
        event.preventDefault();
    }
  
    obtenerAuto(auto){
      this.autoid.marca = auto.marca;
      this.autoid.modelo = auto.modelo;
      this.autoid.monto = auto.monto;
      this.autoid.imagen = auto.imagen;
      this.autoid.color = auto.color;
      this.autoid.estado = auto.estado;
      this.autoid.capacidad = auto.capacidad;
      this.autoid.caracteristicas = auto.caracteristicas;
      this.autoid.placas = auto.placas;
      this.autoid.dias = null;
      this.autoid.id = auto.id;
      this.autoid.fecha = this.fecha;
      console.log(this.autoid);
    }
  
  }
