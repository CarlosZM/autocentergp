import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();
  usuarioActual: UsuarioModel[] = [];
  tipo = localStorage.getItem('tipo');
  admin: boolean;

  constructor( private auth: AuthService,
    private usuariosService: UsuariosService,
    private router: Router ) { }

  ngOnInit(): void {

    console.log('tipo',this.tipo)

    if(this.tipo == "admin") {
      this.admin = true
    } else if (this.tipo = "user") {
      this.admin = false
    }

    console.log('admin',this.admin)

  }

  salir(){
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }

}
