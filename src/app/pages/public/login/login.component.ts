import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UsuarioModel } from 'src/app/models/usuario.model';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { AuthService } from 'src/app/services/auth.service';

import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();
  usuarioActual: UsuarioModel[] = [];
  recordarme = false;

  constructor( private auth: AuthService,
               private usuariosService: UsuariosService,
               private router: Router ) { }

  ngOnInit() {
    if ( localStorage.getItem('email')) {
      this.usuario.email = localStorage.getItem('email');
      this.recordarme = true;
    } else {
      this.usuario.email = "";
    }
  }

  login( form: NgForm ) {

    if ( form.invalid ) { return; }

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();

    this.auth.login( this.usuario )
      .subscribe( resp => {

        console.log(resp);
        Swal.close();

        if ( this.recordarme ) {
          localStorage.setItem('email', this.usuario.email);
        }
        
        this.router.navigateByUrl('/home');

      }, (err) => {

        console.log(err.error.error.message);
        Swal.fire({
          icon: 'error',
          title: 'Error al autenticar',
          text: "Datos incorrectos"
        });
      });

      this.usuariosService.col$('usuarios', ref => ref.where('email','==', this.usuario.email)).subscribe
        ((response:UsuarioModel[]) => {
  
          this.usuarioActual = response;
          
          this.usuarioActual.forEach(DATOS => {
            if (DATOS.nombre) {
              let usuarioDatos = new UsuarioModel
              usuarioDatos.nombre=DATOS.nombre
              usuarioDatos.apellido1=DATOS.apellido1
              usuarioDatos.apellido2=DATOS.apellido2
              usuarioDatos.telefono=DATOS.telefono
              usuarioDatos.direccion=DATOS.direccion
              usuarioDatos.licencia=DATOS.licencia
              usuarioDatos.tipo=DATOS.tipo
              
              this.usuarioActual.push(usuarioDatos)
              console.log("usuario",this.usuarioActual)
              localStorage.setItem('nombre', usuarioDatos.nombre)
              localStorage.setItem('apellido1', usuarioDatos.apellido1)
              localStorage.setItem('apellido2', usuarioDatos.apellido2)
              localStorage.setItem('telefono', (usuarioDatos.telefono).toString())
              localStorage.setItem('direccion', usuarioDatos.direccion)
              localStorage.setItem('licencia', usuarioDatos.licencia)
              localStorage.setItem('tipo', usuarioDatos.tipo)
            }
          })
          console.log("usuarioActual",this.usuarioActual);
        });
  }

}
