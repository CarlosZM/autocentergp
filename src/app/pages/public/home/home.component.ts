import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  usuarioActual: UsuarioModel[] = [];
  nombre = localStorage.getItem('nombre');
  tipo = localStorage.getItem('tipo');
  apellido1 = localStorage.getItem('apellido1');
  apellido2 = localStorage.getItem('apellido2');

  constructor( private auth: AuthService,
               private usuariosService: UsuariosService,
               private router: Router ) { }

  ngOnInit() {
  }

}
