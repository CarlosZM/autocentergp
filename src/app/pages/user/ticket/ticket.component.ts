import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RentaModel } from 'src/app/models/renta.model';
import * as jsPDF from 'jspdf';
import * as html2pdf from 'html2pdf.js'
import { TicketService } from 'src/app/ticket-service.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
})
export class TicketComponent implements OnInit {

  nombre = localStorage.getItem('nombre');
  apellido1 = localStorage.getItem('apellido1');
  apellido2 = localStorage.getItem('apellido2');
  email = localStorage.getItem('email');
  telefono = localStorage.getItem('telefono');
  direccion = localStorage.getItem('direccion');
  licencia = localStorage.getItem('licencia');
  ticket: TicketService;
  rentaRecibida = new RentaModel;

  constructor( private route: Router,
    private ticketService: TicketService,
    private _router: ActivatedRoute) {
      this.ticket = ticketService
  }

  ngOnInit() {
    let recibido
    recibido = window.sessionStorage.getItem("rentaObtenida")
    this.rentaRecibida = JSON.parse(recibido)
    console.log('Recibida',this.rentaRecibida)
  }

  imprimir() {
    const options = {
      filename: 'Ticket_file.pdf',
      image: { type: 'jpg' },
      jspdf: { orienteation: 'landscape' }
    }

    const content: Element = document.getElementById('imp')
    html2pdf().from(content).set(options).save()
  }

  aceptar() {
    this.route.navigate(['...'])
  }

}