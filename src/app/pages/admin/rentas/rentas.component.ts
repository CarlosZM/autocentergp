import { Component, OnInit } from '@angular/core';
import { RentaModel } from 'src/app/models/renta.model'
import { RentasService } from 'src/app/services/rentas.service';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { UsuarioModel } from 'src/app/models/usuario.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-rentas',
  templateUrl: './rentas.component.html',
  styleUrls: ['./rentas.component.css']
})
export class RentasComponent implements OnInit {

  rentas: RentaModel[] = [];
  usuario: UsuarioModel = new UsuarioModel();
  usuarioActual: UsuarioModel = new UsuarioModel();
  cargando: boolean;

  constructor( private rentasService: RentasService,
    private usuariosService: UsuariosService,
    private route: Router ) { }


  rentasR(){

    this.rentasService.col$('rentas')
    .subscribe( listDoc => {
      this.rentas = listDoc;
      this.cargando = false;
      console.log(listDoc)
    });

  }

  ngOnInit(): void {
    this.cargando = true;
    this.rentasR()
  }

  delete(renta){
    Swal.fire({
    title: '¿Eliminar registro?',
    icon: "question",
    showConfirmButton: true,
    showCancelButton: true
    }).then( resp => {

      if ( resp.value ) {
        this.rentasService.delete(`rentas/${renta.id}`)
        .then(() => console.log("Eliminado"))
        .catch(err => console.log(err))

        Swal.fire({
          title: `Registro eliminado`,
          text: 'Datos eliminados',
          icon: 'success'
        });
      }
      
    });
}

}
