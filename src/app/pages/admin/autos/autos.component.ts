import { Component, OnInit } from '@angular/core';
import { AutosService } from 'src/app/services/autos.service';
import { AutoModel } from 'src/app/models/auto.model';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-autos',
  templateUrl: './autos.component.html',
  styleUrls: ['./autos.component.css']
})
export class AutosComponent implements OnInit {

  autos: AutoModel[] = [];
  autoid = new AutoModel;
  cargando: boolean;

  modeloF: String;
  marcaF: String;
  minF: number;
  maxF: number;
  capF: number;

  constructor( private autosService: AutosService ) { }

  ngOnInit(): void {

    this.cargando = true;

    this.autosService.col$('autos')
    .subscribe( listDoc => {
      this.autos = listDoc;
      this.cargando = false;
      console.log(listDoc)
    });

  }

  obtenerAuto(auto) {
    this.autoid.marca = auto.marca;
    this.autoid.modelo = auto.modelo;
    this.autoid.monto = auto.monto;
    this.autoid.imagen = auto.imagen;
    this.autoid.color = auto.color;
    this.autoid.estado = auto.estado;
    this.autoid.capacidad = auto.capacidad;
    this.autoid.caracteristicas = auto.caracteristicas;
    this.autoid.placas = auto.placas;
    this.autoid.id = auto.id;
    console.log(this.autoid);
  }


  delete(auto) {

      Swal.fire({
      title: '¿Eliminar registro?',
      text: `Se eliminara ${ auto.marca } ${ auto.modelo }`,
      icon: "question",
      showConfirmButton: true,
      showCancelButton: true
      }).then( resp => {

        if ( resp.value ) {
          this.autosService.delete(`autos/${auto.id}`)
          .then(() => console.log("Eliminado"))
          .catch(err => console.log(err))

          Swal.fire({
            title: `${ auto.marca } ${ auto.modelo }`,
            text: 'Datos eliminados',
            icon: 'success'
          });
        }
        
      });
  }

  edit(autoid){
    this.autosService.update(`autos/${this.autoid.id}`, this.autoid )
    .then(() => console.log("Actualizado"))
    .catch(err => console.log(err))

    Swal.fire({
      title: `${ this.autoid.marca } ${ this.autoid.modelo }`,
      text: 'Datos guardados',
      icon: 'success'
    });
  }

  getAutos() {
    this.autosService.col$('autos')
    .subscribe( resp => {
      this.autos = resp;
      this.cargando = false;
    });
  }

  filtrarModelo(modeloF){
    this.modeloF=modeloF
    this.autosService.col$('autos', ref => ref.where('modelo','==',this.modeloF))
    .subscribe( resp => {
      console.log(resp);
      this.autos = resp;
      this.cargando = false;
    });
  }

  filtrarMarca(marcaF){
    this.marcaF=marcaF
    this.autosService.col$('autos', ref => ref.where('marca','==',this.marcaF.toUpperCase()))
    .subscribe( resp => {
      console.log(resp);
      this.autos = resp;
      this.cargando = false;
    });
  }

  filtrarMonto(minF,maxF){
    this.minF = minF
    this.maxF = maxF
    this.autosService.col$('autos', ref => ref.where('monto','>', this.minF ).where('monto','<', this.maxF))
    .subscribe( resp => {
      console.log(resp);
      this.autos = resp;
      this.cargando = false;
    });
  }
  
  filtrarCapacidad(capF){
    this.capF = parseInt(capF);
      this.autosService.col$('autos', ref => ref.where('capacidad','==', this.capF))
      .subscribe( resp => {
        console.log(resp);
        this.autos = resp;
        this.cargando = false;
      });
  }

  filtrarDisponibles(){
    this.autosService.col$('autos', ref => ref.where('estado','==', true))
    .subscribe( resp => {
      console.log(resp);
      this.autos = resp;
      this.cargando = false;
    });
  }

  filtrarOcupados(){
    this.autosService.col$('autos', ref => ref.where('estado','==', false))
    .subscribe( resp => {
      console.log(resp);
      this.autos = resp;
      this.cargando = false;
    });
  }

}
