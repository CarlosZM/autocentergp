import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { AutoModel } from 'src/app/models/auto.model';
import { AutosService } from 'src/app/services/autos.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-auto',
  templateUrl: './auto.component.html',
  styleUrls: ['./auto.component.css']
})
export class AutoComponent implements OnInit {

  auto: AutoModel = new AutoModel();

  constructor( private autosService: AutosService,
               private route: ActivatedRoute ) { }

  ngOnInit(): void {

    // const id = this.route.snapshot.paramMap.get('id');

  }

  guardar( form: NgForm ) {

    if ( form.invalid ) {
      console.log( 'Formulario no válido');
      return;
    }

    this.autosService.add( "autos", this.auto )
      .then(response => console.log(response))
      .catch(err => console.log(err));

      Swal.fire({
        title: `${ this.auto.marca } ${ this.auto.modelo }`,
        text: 'Datos guardados',
        icon: 'success'
      });
  }

}
