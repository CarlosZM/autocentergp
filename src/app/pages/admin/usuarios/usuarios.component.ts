import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { UsuarioModel } from 'src/app/models/usuario.model';
import Swal from 'sweetalert2';

import { firebase } from '@firebase/app';
import '@firebase/firestore';
import '@firebase/auth';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuarios: UsuarioModel[] = [];
  usuarioid = new UsuarioModel;
  cargando: boolean;

  constructor( private auth: AuthService,
    private router: Router,
    private usuariosService: UsuariosService ) { }

  ngOnInit(): void {

    this.cargando = true;

    this.usuariosService.col$('usuarios')
    .subscribe( listDoc => {
      this.usuarios = listDoc;
      this.cargando = false;
      console.log(listDoc)
    });
  }

  obtenerUsuario(usuario){
    this.usuarioid.apellido1 = usuario.apellido1;
    this.usuarioid.apellido2 = usuario.apellido2
    this.usuarioid.direccion = usuario.direccion;
    this.usuarioid.email = usuario.email;
    this.usuarioid.id = usuario.id;
    this.usuarioid.licencia = usuario.licencia;
    this.usuarioid.nombre = usuario.nombre;
    this.usuarioid.password = usuario.password;
    this.usuarioid.telefono = usuario.telefono;
    // this.usuarioid.tipo = usuario.tipo;
    console.log(this.usuarioid);
  }

  delete(usuario){
    Swal.fire({
      title: '¿Eliminar registro?',
      text: `Se eliminara ${ usuario.nombre } ${ usuario.apellido1 } ${ usuario.apellido2 }`,
      icon: "question",
      showConfirmButton: true,
      showCancelButton: true
      }).then( resp => {

        if ( resp.value ) {
          this.usuariosService.delete(`usuarios/${usuario.id}`)
          .then(() => console.log("Eliminado"))
          .catch(err => console.log(err))
        }
      });
  }

  edit(usuarioid){
    this.usuariosService.update(`usuarios/${this.usuarioid.id}`, this.usuarioid )
    .then(() => console.log("Actualizado"))
    .catch(err => console.log(err))
  }

}
