import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RentaModel } from '../models/renta.model';
import { map, delay } from 'rxjs/operators';
import { UsuarioModel } from '../models/usuario.model';
import { UsuariosService } from './usuarios.service';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AutoModel } from '../models/auto.model';

type CollectionPredicate<T> = string | AngularFirestoreCollection;
type DocumentPredicate<T> = string | AngularFirestoreDocument;

@Injectable({
  providedIn: 'root'
})
export class RentasService {

  private url = 'https://autocenter-35144.firebaseio.com';

  user = localStorage.getItem('email');
  usuarioActual: UsuarioModel[] = [];
  fecha: Date = new Date();

  constructor( private http: HttpClient,
    private usuariosService: UsuariosService,
    private afs:AngularFirestore) { }

  private col<T>(ref:CollectionPredicate<T>, queryFn?): AngularFirestoreCollection{
    return typeof ref === "string"? this.afs.collection(ref, queryFn): ref;
  }

  private doc<T>(ref:DocumentPredicate<T>): AngularFirestoreDocument{
    return typeof ref === "string"? this.afs.doc(ref) : ref;
  }

  col$<T>(ref:CollectionPredicate<T>, queryFn?):Observable<any[]>{
    return this.col(ref, queryFn).snapshotChanges().pipe(
      map(docs => {
        return docs.map(d => {
          const data = d.payload.doc.data();
          const id = d.payload.doc.id;
          return { id, ...data}
        })
      })
    )
  }

  crearRenta<T>(ref:CollectionPredicate<T>, auto){
    console.log(auto)
    return this.col(ref).add(
      {
        email: localStorage.getItem('email'),
        nombre: localStorage.getItem('nombre'),
        apellido1: localStorage.getItem('apellido1'),
        apellido2: localStorage.getItem('apellido2'),
        telefono: localStorage.getItem('telefono'),
        direccion: localStorage.getItem('direccion'),
        licencia: localStorage.getItem('licencia'),
        capacidad: auto.capacidad,
        caracteristicas: auto.caracteristicas,
        color: auto.color,
        dias: auto.dias,
        fecha: (this.fecha).toString(),
        marca: auto.marca,
        modelo: auto.modelo,
        monto: auto.monto,
        placas: auto.placas
      }
    )
  }

  delete<T>(ref:DocumentPredicate<T>){
    return this.doc(ref).delete();
  }

}
