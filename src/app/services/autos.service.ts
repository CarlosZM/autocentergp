import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AutoModel } from '../models/auto.model';
import { map, delay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

type CollectionPredicate<T> = string | AngularFirestoreCollection;
type DocumentPredicate<T> = string | AngularFirestoreDocument;

@Injectable({
  providedIn: 'root'
})
export class AutosService {

  private url = 'https://autocenter-35144.firebaseio.com';

  constructor( private http: HttpClient,
    private afs:AngularFirestore ) { }

  private col<T>(ref:CollectionPredicate<T>, queryFn?): AngularFirestoreCollection{
    return typeof ref === "string"? this.afs.collection(ref, queryFn): ref;
  }

  private doc<T>(ref:DocumentPredicate<T>): AngularFirestoreDocument{
    return typeof ref === "string"? this.afs.doc(ref) : ref;
  }

  col$<T>(ref:CollectionPredicate<T>, queryFn?):Observable<any[]>{
    return this.col(ref, queryFn).snapshotChanges().pipe(
      map(docs => {
        return docs.map(d => {
          const data = d.payload.doc.data();
          const id = d.payload.doc.id;
          return { id, ...data}
        })
      })
    )
  }

  getAuto( id: string ) {
    return this.http.get(`${ this.url }/autos/${ id }.json`);
  }

  add<T>(ref:CollectionPredicate<T>, data){
    return this.col(ref).add({
      ...data
    })
  }

  update<T>(ref:DocumentPredicate<T>, data){
    return this.doc(ref).update({
      ...data
    })
  }

  updateEstado<T>(ref:DocumentPredicate<T>, data){
    return this.doc(ref).update({
      ...data,
      estado: false
    })
  }

  delete<T>(ref:DocumentPredicate<T>){
    return this.doc(ref).delete();
  }

}
